from flask import Flask, jsonify,request
from flasgger import Swagger
from models import Tweets

app = Flask(__name__)
swagger = Swagger(app)

@app.route('/sentiment/feature/',methods=['POST'])
def sentiment():
    """Here is an api of Feature based sentiment analysis of any textual data.
    ---
    parameters:
      - name: text
        in: body
        type: string
        required: true
        description: The text you want to analyse
    definitions:
      Text:
        type: object
        properties:
          text_name:
            type: string
            items:
              $ref: '#/definitions/sentiment'
      Color:
        type: string
    
    """
    return jsonify(Tweets().get_sentiment_alt(str(request.data)))

app.run(debug=True)