from __future__ import division
import nltk
import random
from nltk.tokenize import word_tokenize
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression,SGDClassifier
from sklearn.svm import NuSVC
from nltk.classify import ClassifierI
from statistics import mode
from nltk.corpus import stopwords
import warnings
import os

warnings.filterwarnings("ignore")

stopWords = set(stopwords.words("english"))

class VoteClassifier(ClassifierI):
    def __init__(self, *classifiers):
        self._classifiers = classifiers

    def classify(self, features):
        votes = []
        for c in self._classifiers:
            v = c.classify(features)
            votes.append(v)
        return mode(votes)

    def confidence(self, features):
        votes = []
        for c in self._classifiers:
            v = c.classify(features)
            votes.append(v)

        choice_votes = votes.count(mode(votes))
        conf = choice_votes / len(votes)
        '''
        print "Votes of all the classifiers :"
        print votes
        print 'Mode of votes :',mode(votes)
        print 'No. of Choice votes :',choice_votes
        print 'Total votes :',len(votes)
        print 'Confidence :',conf
        '''
        return conf


module_dir = os.path.dirname(__file__)
file_path = os.path.join(module_dir, 'static/datasets/imdb_labelled_utf_8.txt')
#file_path = os.path.join(module_dir, 'static/datasets/amazon_cells_labelled.txt')

txt_data = open(file_path,"r").read()
txt_data = str(txt_data).decode('utf-8').encode('utf-8').lower()

data = []
short_pos = []
short_neg = []
all_words = []

for p in txt_data.split('\n')[0:999]:

    if(len(p.split('\t'))>1):
        i,j = p.split('\t')
    else:
        continue
    if(j==1 or j=='1'):
        data.append((str(i).strip(),"pos"))
        for w in word_tokenize(p):
            short_pos.append(w)
            all_words.append(w)
    else:
        data.append((str(i).strip(),"neg"))
        for w in word_tokenize(p):
            short_neg.append(w)
            all_words.append(w)

all_words = nltk.FreqDist(all_words)

word_features = list(all_words.keys())

def find_features(document):
    words = word_tokenize(document)
    features = {}
    for w in word_features:
        features[w] = (w in words)

    return features

featuresets = [(find_features(rev), category) for (rev, category) in data]

random.shuffle(featuresets)

##training_set = featuresets[:(int(len(featuresets)/2))]
##testing_set =  featuresets[(int(len(featuresets)/2)):]

training_set = featuresets

classifier = nltk.NaiveBayesClassifier.train(training_set)
#classifier.show_most_informative_features(15)

LogisticRegression_classifier = SklearnClassifier(LogisticRegression())
LogisticRegression_classifier.train(training_set)

SGDClassifier_classifier = SklearnClassifier(SGDClassifier())
SGDClassifier_classifier.train(training_set)

MNB_classifier = SklearnClassifier(MultinomialNB())
MNB_classifier.train(training_set)

NuSVC_classifier = SklearnClassifier(NuSVC())
NuSVC_classifier.train(training_set)


##BernoulliNB_classifier = SklearnClassifier(BernoulliNB())
##BernoulliNB_classifier.train(training_set)
##print("BernoulliNB_classifier accuracy percent:", (nltk.classify.accuracy(BernoulliNB_classifier, testing_set))*100)

##print("Original Naive Bayes Algo accuracy percent:", (nltk.classify.accuracy(classifier, testing_set))*100)
##print("LogisticRegression_classifier accuracy percent:", (nltk.classify.accuracy(LogisticRegression_classifier, testing_set))*100)
##print("SGD_classifier accuracy percent:", (nltk.classify.accuracy(SGDClassifier_classifier, testing_set))*100)
##print("MNB_classifier accuracy percent:", (nltk.classify.accuracy(MNB_classifier, testing_set))*100)
##print("NuSVC_classifier accuracy percent:", (nltk.classify.accuracy(NuSVC_classifier, testing_set))*100)

voted_classifier = VoteClassifier(classifier,
                                  NuSVC_classifier,
                                  SGDClassifier_classifier,
                                  MNB_classifier,
                                  LogisticRegression_classifier)


def sentiment(text):
    feats = find_features(text)
    '''print "Review",text'''
    return voted_classifier.classify(feats),voted_classifier.confidence(feats)
