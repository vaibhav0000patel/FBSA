from __future__ import unicode_literals
from __future__ import division
from django.http import HttpResponse
from django.shortcuts import render
from .models import Tweets
import json

def index(request):

	context={}
	
	if request.method == 'GET' and 'tw_search' in request.GET:
		tw_search = request.GET['tw_search']
		tw_count = request.GET['tw_count'] if 'tw_count' in request.GET else 100
		processed_tweets_data,pos_tweets,neg_tweets,neu_tweets,feature_set = Tweets.get_all_tweets(Tweets(), tw_search, tw_count)
		context = {
			'tweets': processed_tweets_data,
			'feature_set':feature_set.items(),
			'is_post':True,
			'tw_search_result':tw_search,
			'tw_total_tweets':pos_tweets+neg_tweets+neu_tweets,
			'positive_tweets':pos_tweets,
			'negative_tweets':neg_tweets,
			'neutral_tweets':neu_tweets,
		}
	else:
		context = {
			'tweets': '',
			'is_post':False,
			'trends':Tweets.get_trends(Tweets())
		}

	return render(request,'twitter/index.html',context)

def demo(request):
	context = {}
	sent_data,pos_tweets,neg_tweets,neu_tweets,feature_set = Tweets.generate_fbsa_demo(Tweets())

	context.update({
		'tweets': sent_data,
		'feature_set':feature_set.items(),
		'tw_search_result':'Moto G5s',
		'tw_total_tweets':pos_tweets+neg_tweets+neu_tweets,
		'positive_tweets':pos_tweets,
		'negative_tweets':neg_tweets,
		'neutral_tweets':neu_tweets,
		'count':0
	})
	return render(request,'twitter/demo.html',context)
