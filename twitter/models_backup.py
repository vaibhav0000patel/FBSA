# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import division
import tweepy
import json
import sys
from django.db import models
import re
from nltk.corpus import stopwords
import pickle
import nltk
from nltk.classify import ClassifierI
from statistics import mode
from nltk.tokenize import word_tokenize
stopWords = set(stopwords.words("english"))
import os
settings_dir = os.path.dirname(__file__)
PROJECT_ROOT = os.path.abspath(os.path.dirname(settings_dir))


# Create your models here.
classifier = None
MultinomialNB_classifier = None
BernoulliNB_classifier = None
LogisticRegression_classifier = None
LinearSVC_classifier = None
SGDClassifier_classifier = None
NuSVC_classifier = None
word_features = None
voted_classifier = None


class VoteClassifier(ClassifierI):
	def __init__(self, *classifiers):
		self._classifiers = classifiers

	def classify(self, features):
		votes = []
		for c in self._classifiers:
			v = c.classify(features)
			votes.append(v)
		return mode(votes)

	def confidence(self, features):
		votes = []
		for c in self._classifiers:
			v = c.classify(features)
			votes.append(v)

		choice_votes = votes.count(mode(votes))
		conf = choice_votes / len(votes)

		return conf

def load_all_classifiers(tw_type):
    global classifier
    global MultinomialNB_classifier
    global BernoulliNB_classifier
    global LogisticRegression_classifier
    global LinearSVC_classifier
    global SGDClassifier_classifier
    global NuSVC_classifier
    global word_features
    global voted_classifier


    path = os.path.join(PROJECT_ROOT, "twitter/static/pickled_algos/"+tw_type+"/originalnaivebayes.pickle")
    load_classifier = open(path, "rb")
    classifier = pickle.load(load_classifier)
    load_classifier.close()
    print "NAIVE-BAYES CLASSIFIER loaded..."

    path = os.path.join(PROJECT_ROOT, "twitter/static/pickled_algos/"+tw_type+"/MNB_classifier.pickle")
    load_MultinomialNB_classifier = open(path, "rb")
    MultinomialNB_classifier = pickle.load(load_MultinomialNB_classifier)
    load_MultinomialNB_classifier.close()
    print "MultinomialNB_classifier loaded..."

    path = os.path.join(PROJECT_ROOT, "twitter/static/pickled_algos/"+tw_type+"/BernoulliNB_classifier.pickle")
    load_BernoulliNB_classifier5k = open(path, "rb")
    BernoulliNB_classifier = pickle.load(load_BernoulliNB_classifier5k)
    load_BernoulliNB_classifier5k.close()
    print "BernoulliNB_classifier loaded..."

    path = os.path.join(PROJECT_ROOT, "twitter/static/pickled_algos/"+tw_type+"/LogisticRegression_classifier.pickle")
    load_LogisticRegression_classifier = open(path, "rb")
    LogisticRegression_classifier = pickle.load(load_LogisticRegression_classifier)
    load_LogisticRegression_classifier.close()
    print "LogisticRegression_classifier loaded..."

    path = os.path.join(PROJECT_ROOT, "twitter/static/pickled_algos/"+tw_type+"/LinearSVC_classifier.pickle")
    load_LinearSVC_classifier = open(path, "rb")
    LinearSVC_classifier = pickle.load(load_LinearSVC_classifier)
    load_LinearSVC_classifier.close()
    print "LinearSVC_classifier loaded..."


    path = os.path.join(PROJECT_ROOT, "twitter/static/pickled_algos/"+tw_type+"/SGDC_classifier.pickle")
    load_SGDClassifier_Classifier = open(path, "rb")
    SGDClassifier_classifier = pickle.load(load_SGDClassifier_Classifier)
    load_SGDClassifier_Classifier.close()
    print "SGDClassifier_classifier loaded..."

    path = os.path.join(PROJECT_ROOT, "twitter/static/pickled_algos/"+tw_type+"/NuSVC_classifier.pickle")
    load_NuSVC_classifier = open(path, "rb")
    NuSVC_classifier = pickle.load(load_NuSVC_classifier)
    load_NuSVC_classifier.close()
    print "NuSVC_classifier loaded..."


def load_word_features(tw_type):
	global word_features

	path = os.path.join(PROJECT_ROOT, "twitter/static/pickled_items/"+tw_type+"_word_features.pickle")
	load_word_features = open(path, "rb")
	word_features = pickle.load(load_word_features)
	load_word_features.close()

def set_voted_classifier():
	global voted_classifier

	if(classifier!=None):
		voted_classifier = VoteClassifier(classifier,
                                          MultinomialNB_classifier,
                                          BernoulliNB_classifier,
                                          NuSVC_classifier,
                                          LinearSVC_classifier,
                                          SGDClassifier_classifier,
                                          LogisticRegression_classifier)
def processTweet(tweet):
    tweet = tweet.lower()
    tweet = re.sub('((www\.[^\s]+)|(https?://[^\s]+))','URL',tweet)
    tweet = re.sub('@[^\s]+','AT_USER',tweet)
    tweet = re.sub('[\s]+', ' ', tweet)
    tweet = re.sub(r'#([^\s]+)', r'\1', tweet)
    tweet = tweet.strip('\'"')
    return tweet

def replaceTwoOrMore(s):
    pattern = re.compile(r"(.)\1{1,}", re.DOTALL)
    return pattern.sub(r"\1\1", s)

def getFeatureVector(tweet):
    featureVector = []
    words = word_tokenize(tweet.encode('utf-8', 'ignore').decode('utf-8'))
    for w in words:
        w = replaceTwoOrMore(w)
        w = w.strip('\'"?,.')
        val = re.search(r"^[a-zA-Z][a-zA-Z0-9]*$", w)
        if(w in stopWords or val is None):
            continue
        else:
            featureVector.append(w.lower())
    return featureVector

def find_features(document):
	global word_features

	words = getFeatureVector(document)
	features = {}
	for w in word_features:
		features[w] = (w in words)
	return features

def sentiment(text):
	global voted_classifier
	feats = find_features(processTweet(text))
	if(voted_classifier!=None):
		return voted_classifier.classify(feats),voted_classifier.confidence(feats)
	else:
		return "Null",0

class Tweets():
#class Tweets(models.Model):
    def get_all_tweets(self,query,tweet_count,tw_type):
        ckey = "Lxid3K1FZcJ2vVj4JO6rikvAA"
        csecret = "BQhNsJMiqo3sBq37atjie7h8MbAoCoPMDG7iz90BfbjLwjYmil"
        atoken = "2973557882-39g5RhcBl2scFt4u4Nwnczxw6kzC6yWalMoa9PH"
        asecret = "8IHj8PnChoy8DrYMOltE3qWZjcnbzRiJBmm2CGj0ao2bv"

        auth = tweepy.OAuthHandler(ckey, csecret)
        auth.set_access_token(atoken, asecret)

        api = tweepy.API(auth)
        tweets_data = []
        load_all_classifiers(tw_type)
        load_word_features(tw_type)
        set_voted_classifier()
        count = 0
        processed_tweets_data = []
        pos_tweets = 0
        neg_tweets = 0
        num_tweets = 0

        #while(count<tweet_count):

        tweets_data = api.search(q=query, count=100, lang="en", result_type="mixed", tweet_mode="extended")

        for tw in tweets_data:
            sentiment_value, confidence = sentiment(tw.full_text)
            processed_tweets_data.append((tw.full_text, sentiment_value, confidence))
            if (sentiment_value == "pos"):
                pos_tweets += 1
            else:
                neg_tweets += 1

            #count += 1
            num_tweets += 1

        return processed_tweets_data,pos_tweets,neg_tweets,num_tweets

    def unit_test_sentiment(self, tweets_data, tw_type):

        load_all_classifiers(tw_type)
        load_word_features(tw_type)
        set_voted_classifier()

        processed_tweets_data = []
        pos_tweets = 0
        neg_tweets = 0
        num_tweets = 0

        for tw in tweets_data:
            sentiment_value, confidence = sentiment(tw)
            processed_tweets_data.append((tw, sentiment_value, confidence))
            if (sentiment_value == "pos"):
                pos_tweets += 1
            else:
                neg_tweets += 1
            num_tweets += 1

        return {
            "tweets":processed_tweets_data,
            "ptw":pos_tweets,
            "ntw":neg_tweets,
            "ttw":num_tweets
        }
