# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import division
import tweepy
import json
import sys
from django.db import models
from nltk.tokenize import sent_tokenize
from textblob import TextBlob
import psycopg2

#class Tweets(models.Model):
class Tweets():

	def get_features(self,review,feature_set):
		tb_obj = TextBlob(review)
		tb_obj.correct()
		for sent in tb_obj.sentences:
			sentiment = sent.sentiment.polarity
			for noun in [tag for tag in sent.tags if str(tag[1])=="NN" or str(tag[1])=="NNP"]:
				if noun[0] not in feature_set:
					feature_set[noun[0]] = [0,0,0]
				if sentiment>0:
					feature_set[noun[0]][0] += 1
				elif sentiment<0:
					feature_set[noun[0]][1] += 1
				else:
					feature_set[noun[0]][2] += 1
				feature_set['nouns_count']+=1

		return feature_set

	def filter_feature(self,feature_set,n):
		feature_set_final={}
		nouns_count = 0
		for key in feature_set:
			if key != 'nouns_count':
			    if [i for i in feature_set[key] if (i>=n)]:
			        feature_set_final[key] = feature_set[key]
			else:
				nouns_count = feature_set['nouns_count']
		return feature_set_final,nouns_count

	def get_reviews_from_database(self):
		conn = psycopg2.connect(host="localhost",database="fbsa", user="root", password="123")
		cur = conn.cursor()
		cur.execute("Select * from dataset")
		data = cur.fetchall()
		cur.close()
		conn.close()
		return data

	def get_sentiment_analysis(self,data,flag):
		processed_tweets_data = []
		pos_tweets = 0
		neg_tweets = 0
		neu_tweets = 0

		for tw in data:
			if flag=="Twitter":
				tb_tweet = TextBlob(tw.full_text)
				tweet = tw.full_text
			elif flag=="Demo":
				tb_tweet = tw[0]
			else:
				tb_tweet = False

			if tb_tweet:
				if flag=="Twitter":
					sentiment_value = tb_tweet.sentiment.polarity
					processed_tweets_data.append((tweet, sentiment_value))
					if (sentiment_value>0):
						pos_tweets += 1
					elif (sentiment_value<0):
						neg_tweets += 1
					else:
						neu_tweets += 1
				elif flag=="Demo":
					for sent in sent_tokenize(tb_tweet):
						sent_tb = TextBlob(sent)
						sentiment_value = sent_tb.sentiment.polarity
						processed_tweets_data.append((sent, sentiment_value))
						if (sentiment_value>0):
							pos_tweets += 1
						elif (sentiment_value<0):
							neg_tweets += 1
						else:
							neu_tweets += 1

		return processed_tweets_data,pos_tweets,neg_tweets,neu_tweets

	def generate_fbsa_demo(self):
		all_data = self.get_reviews_from_database()
		feature_set = {}
		feature_set['nouns_count'] = 0
		for data in all_data:
			feature_set = self.get_features(data[0],feature_set)
		feature_set,nouns_count = self.filter_feature(feature_set,2)
		sent_data,pos_tweets,neg_tweets,neu_tweets =  self.get_sentiment_analysis(all_data,"Demo")
		return sent_data,pos_tweets,neg_tweets,neu_tweets,feature_set

	def get_all_tweets(self,query,tweet_count):
		ckey = "wJRGRDnAyiKQobBKiNBSdWCbl"
		csecret = "y4EplrVNbMN27Wl8kCCVMaLCKghtZCzBs10DwpJzlYmHlcepLY"
		atoken = "2973557882-39g5RhcBl2scFt4u4Nwnczxw6kzC6yWalMoa9PH"
		asecret = "8IHj8PnChoy8DrYMOltE3qWZjcnbzRiJBmm2CGj0ao2bv"

		auth = tweepy.OAuthHandler(ckey, csecret)
		auth.set_access_token(atoken, asecret)
		api = tweepy.API(auth)

		count = 0
		searched_tweets = []
		last_id = -1
		max_tweets = int(tweet_count)

		while len(searched_tweets) < max_tweets:
			count = max_tweets - len(searched_tweets)
			try:
				new_tweets = api.search(q=query, count=count, max_id=str(last_id - 1), lang="en", result_type="mixed", tweet_mode="extended")
				if not new_tweets:
					break
				searched_tweets.extend(new_tweets)
				last_id = new_tweets[-1].id
			except tweepy.TweepError as e:
				break

		return self.get_sentiment_analysis(searched_tweets,"Twitter")
		#return [],0,0,0
