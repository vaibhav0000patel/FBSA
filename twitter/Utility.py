# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import division
from tagger.tagger import *
from tagger.extras import UnicodeReader
import os
import pickle
from textblob import TextBlob
from nltk.tokenize.punkt import PunktSentenceTokenizer
import re
from nltk.corpus import stopwords
tokenizer = PunktSentenceTokenizer()

stopWords = set(stopwords.words("english"))

PATH = os.path.expanduser('/opt/odoo/odoo_addons/fbsa/tagger/data/dict.pkl')
with open(PATH, 'rb') as f:
    weights = pickle.load(f)
myTagger = Tagger(UnicodeReader(), Stemmer(), Rater(weights))

class Utility(object):
    def get_tags(self,msg):
        if msg:
            tags = myTagger(str(msg))
            return tags
    
    def get_sentiment(self,msg):
        
        data = {
            'features':{},
            }
        overall_pol = 0.0
        tok_msg = tokenizer.tokenize(msg)
        for sent in tok_msg:
            sent_tb = TextBlob(sent)
            pol = sent_tb.sentiment.polarity
            overall_pol += pol
            for tag in self.get_tags(sent):
                if tag not in data['features']:
                    data['features'][tag] = [0,0,0]
                if pol>0:
                    data['features'][tag][0] += 1
                elif pol<0:
                    data['features'][tag][1] += 1
                else:
                    data['features'][tag][2] += 1
        overall_pol = overall_pol/len(tok_msg)
        if overall_pol>0:
            overall_sent = 'pos'    
        elif overall_pol<0:
            overall_sent = 'neg'
        else:
            overall_sent = 'neu'
        data.update({
            'overall_pol':overall_pol,
            'overall_sent':overall_sent
        })
        return data

    def get_sentiment_alt(self,msg):
        data = {
            'features':{},
            }
        sent_tb = TextBlob(msg)
        overall_pol = sent_tb.sentiment.polarity
        for tag in self.get_tags(msg):
            tag = re.sub(r"[^a-zA-Z0-9]+", ' ', str(tag))
            tag = tag.strip().replace(' ','_')
            if len(tag)>2 and tag not in stopWords:
                if tag not in data['features']:
                    data['features'][tag] = [0,0,0]
                if overall_pol>0:
                    data['features'][tag][0] += 1
                elif overall_pol<0:
                    data['features'][tag][1] += 1
                else:
                    data['features'][tag][2] += 1
        if overall_pol>0:
            overall_sent = 'pos'    
        elif overall_pol<0:
            overall_sent = 'neg'
        else:
            overall_sent = 'neu'
        data.update({
            'overall_pol':overall_pol,
            'overall_sent':overall_sent
        })
        return data