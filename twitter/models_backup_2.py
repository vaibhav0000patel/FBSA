# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import division
import tweepy
import json
import sys
from django.db import models
from textblob import TextBlob
import re

def processTweet(tweet):
    tweet = tweet.lower()
    tweet = re.sub('((www\.[^\s]+)|(https?://[^\s]+))','URL',tweet)
    tweet = re.sub('@[^\s]+','AT_USER',tweet)
    tweet = re.sub('[\s]+', ' ', tweet)
    tweet = re.sub(r'#([^\s]+)', r'\1', tweet)
    tweet = tweet.strip('\'"')
    return tweet

class Tweets(models.Model):
	def get_all_tweets(self,query,tweet_count):
		ckey = "Lxid3K1FZcJ2vVj4JO6rikvAA"
		csecret = "BQhNsJMiqo3sBq37atjie7h8MbAoCoPMDG7iz90BfbjLwjYmil"
		atoken = "2973557882-39g5RhcBl2scFt4u4Nwnczxw6kzC6yWalMoa9PH"
		asecret = "8IHj8PnChoy8DrYMOltE3qWZjcnbzRiJBmm2CGj0ao2bv"

		auth = tweepy.OAuthHandler(ckey, csecret)
		auth.set_access_token(atoken, asecret)
		api = tweepy.API(auth)
		tweets_data = []
		count = 0
		processed_tweets_data = []
		pos_tweets = 0
		neg_tweets = 0
		neu_tweets = 0
		num_tweets = 0
		features = {}
		nouns_count = 0

		while tweet_count > num_tweets:
			remaining_tweets = int(tweet_count) - int(num_tweets)
			if remaining_tweets>0:
				tweets_data = api.search(q=query, count=remaining_tweets, lang="en", result_type="mixed", tweet_mode="extended")

				for tw in tweets_data:
					tb_tweet = TextBlob(processTweet(tw.full_text))
					tb_tweet.correct()
					sentiment_value = tb_tweet.sentiment.polarity
					processed_tweets_data.append((tw.full_text, sentiment_value))
					if (sentiment_value>0):
						pos_tweets += 1
					elif (sentiment_value<0):
						neg_tweets += 1
					else:
						neu_tweets += 1
					num_tweets += 1
					'''
					for sent in tb_tweet.sentences:
						sentiment = sent.sentiment.polarity
						for noun in [tag for tag in sent.tags if ((str(tag[1])=="NN" or str(tag[1])=="NNP") and len(str(tag[1]))>1 and ('/' not in tag[1]))]:
							if noun[0] not in features:
								features[noun[0]] = [0,0,0]
							if sentiment>0:
								features[noun[0]][0] += 1
							elif sentiment<0:
								features[noun[0]][1] += 1
							else:
								features[noun[0]][2] += 1
							nouns_count+=1
				for key in features:
					if [i for i in features[key] if i>2]:
						print key,":",features[key]
				print "Nouns",nouns_count
				'''
			else:
				break
		return processed_tweets_data,pos_tweets,neg_tweets,neu_tweets,num_tweets
