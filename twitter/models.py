# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import division
import tweepy
import json
import sys
from django.db import models
from nltk.tokenize import sent_tokenize
from textblob import TextBlob
import psycopg2
from nltk.tokenize.punkt import PunktSentenceTokenizer
from tagger.tagger import *
from tagger.extras import UnicodeReader
import os
import pickle
import re
from nltk.corpus import stopwords
import urllib

tokenizer = PunktSentenceTokenizer()

PATH = os.path.expanduser('/opt/odoo/odoo_addons/fbsa/tagger/data/dict.pkl')
with open(PATH, 'rb') as f:
    weights = pickle.load(f)
myTagger = Tagger(UnicodeReader(), Stemmer(), Rater(weights))

stopWords = set(stopwords.words("english"))


ckey = "wJRGRDnAyiKQobBKiNBSdWCbl"
csecret = "y4EplrVNbMN27Wl8kCCVMaLCKghtZCzBs10DwpJzlYmHlcepLY"
atoken = "2973557882-39g5RhcBl2scFt4u4Nwnczxw6kzC6yWalMoa9PH"
asecret = "8IHj8PnChoy8DrYMOltE3qWZjcnbzRiJBmm2CGj0ao2bv"

auth = tweepy.OAuthHandler(ckey, csecret)
auth.set_access_token(atoken, asecret)
twapi = tweepy.API(auth)

#class Tweets(models.Model):
class Tweets():

	def get_tags(self,msg):
		if msg:
			tags = myTagger(msg)
			return tags

	def get_sentiment(self,msg):
        
		data = {'features':{}}
		overall_pol = 0.0
		tok_msg = tokenizer.tokenize(msg)
		for sent in tok_msg:
			sent_tb = TextBlob(sent)
			pol = sent_tb.sentiment.polarity
			overall_pol += pol
			for tag in self.get_tags(sent):
				if tag not in data['features']:
					data['features'][tag] = [0,0,0]
				if pol>0:
					data['features'][tag][0] += 1
				elif pol<0:
					data['features'][tag][1] += 1
				else:
					data['features'][tag][2] += 1
		overall_pol = overall_pol/len(tok_msg)
		if overall_pol>0:
			overall_sent = 'pos'    
		elif overall_pol<0:
			overall_sent = 'neg'
		else:
			overall_sent = 'neu'
		data.update({
			'overall_pol':overall_pol,
			'overall_sent':overall_sent
		})
		return data

	def get_sentiment_alt(self,msg):
		data = {
			'features':{},
			}
		sent_tb = TextBlob(msg)
		overall_pol = sent_tb.sentiment.polarity
		for tag in self.get_tags(msg):
			tag = re.sub(r"[^a-zA-Z0-9]+", ' ', str(tag))
			tag = tag.strip()
			if len(tag)>2 and tag not in stopWords:
				if tag not in data['features']:
					data['features'][tag] = [0,0,0]
				if overall_pol>0:
					data['features'][tag][0] += 1
				elif overall_pol<0:
					data['features'][tag][1] += 1
				else:
					data['features'][tag][2] += 1
		if overall_pol>0:
			overall_sent = 'pos'
		elif overall_pol<0:
			overall_sent = 'neg'
		else:
			overall_sent = 'neu'
		data.update({
			'overall_pol':overall_pol,
			'overall_sent':overall_sent
		})
		return data
	
	def filter_feature(self,feature_set):
		feature_set_final={}
		for key in feature_set:
			if [i for i in feature_set[key] if (i>1)]:
			    feature_set_final[key] = feature_set[key]
		return feature_set_final

	def get_reviews_from_database(self):
		conn = psycopg2.connect(host="localhost",database="fbsa", user="root", password="123")
		cur = conn.cursor()
		cur.execute("Select * from dataset")
		data = cur.fetchall()
		cur.close()
		conn.close()
		return data

	def get_sentiment_analysis(self,data,flag):
		processed_tweets_data = []
		pos_tweets = 0
		neg_tweets = 0
		neu_tweets = 0
		features = {}
		for tw in data:
			if flag=="Twitter":
				tweet = tw.full_text
			elif flag=="Demo":
				tweet = tw[0]
			else:
				tweet = False
			if tweet:
				tok_msg = tokenizer.tokenize(tweet)
				for sent in tok_msg:
					data = self.get_sentiment_alt(sent)
					processed_tweets_data.append((sent,data['overall_pol']))
					if (data['overall_pol']>0):
						pos_tweets += 1
					elif (data['overall_pol']<0):
						neg_tweets += 1
					else:
						neu_tweets += 1
					for key in data['features']:
						if key not in features:
							features[key] = data['features'][key]
						else:
							features[key][0] += data['features'][key][0]
							features[key][1] += data['features'][key][1]
							features[key][2] += data['features'][key][2]
		if flag=="Twitter":
			features = self.filter_feature(features)
		return processed_tweets_data,pos_tweets,neg_tweets,neu_tweets,features
			

	def generate_fbsa_demo(self):
		all_data = self.get_reviews_from_database()
		return self.get_sentiment_analysis(all_data,"Demo")

	def get_all_tweets(self,query,tweet_count):
		count = 0
		searched_tweets = []
		last_id = -1
		max_tweets = int(tweet_count)
		
		while len(searched_tweets) < max_tweets:
			count = max_tweets - len(searched_tweets)
			print "MAX",max_tweets
			print "search",len(searched_tweets)
			print "count",count
			if count==0:
				break
			try:
				new_tweets = twapi.search(q=query, count=count, max_id=str(last_id - 1), lang="en", result_type="mixed", tweet_mode="extended")
				if not new_tweets:
					break
				searched_tweets.extend(new_tweets)
				last_id = new_tweets[-1].id
			except tweepy.TweepError as e:
				break

		return self.get_sentiment_analysis(searched_tweets,"Twitter")
		#return [],0,0,0

	def get_trends(self):
		#trends = twapi.trends_available()
		trends_loc = twapi.trends_closest(23.0862715,72.5792914)
		trends = twapi.trends_place(trends_loc[0]['woeid'])
		trends_data = []
		for tr in [trend for trend in trends[0]['trends']]:
			trends_data.append((tr['query'],urllib.unquote(tr['query'].replace('+',' '))))
		return trends_data